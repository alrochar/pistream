

## Code test

# This code test has the following implementation

- Developed the requested bugs and features

- Migrated to Swift

- Requests data, and adds all the indexes of the 3 number into the rows

- Persistance of the data between sessions

- Available to delete all the data 

- Point 4 of the features not completed, I haven't find a native way to poll data nor open a socket connection, and update the tableView in real time.
