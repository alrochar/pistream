//
//  HTTPTask.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 05/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import Foundation

enum Result<T> {
    
    case success(T)
    case failure(Error)
    case empty
}

class ServiceProvider<T: Service> {
    
    var task: URLSessionTask?
        
    func load(service: T, completion: @escaping (Result<Any>) -> Void) {
        
        self.request(service.urlRequest) { result in
            
            switch result {
            case .success(let data):
                
                if let resp = String(data: data, encoding: .utf8) {
                    completion(.success(resp))
                }
                
            case .failure(let error):
                completion(.failure(error))
                
            case .empty:
                completion(.empty)
            }
        }
    }
    
    func cancel() {
        
        self.task?.cancel()
    }
}

extension ServiceProvider {
    
    private func request(_ request: URLRequest, deliverQueue: DispatchQueue = DispatchQueue.main, completion: @escaping (Result<Data>) -> Void) {
        
        let urlSession = URLSession.shared
        
        self.task = urlSession.dataTask(with: request) { (data, _, error) in
            
            if let error = error {
                deliverQueue.async {
                    completion(.failure(error))
                }
            } else if let data = data {
                deliverQueue.async {
                    completion(.success(data))
                }
            } else {
                deliverQueue.async {
                    completion(.empty)
                }
            }
        }
        
        self.task?.resume()
    }
}
