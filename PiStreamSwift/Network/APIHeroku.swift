//
//  APIHeroku.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 05/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import Foundation

protocol APIDataDelegate {
    
    func requestData(offset: String?, duration: String)
    func cancelRequest()
}

enum APIHeroku {
 
    case offset(offset: String?)
}

extension APIHeroku: Service {
    
    var baseURL: String {
        
        return "http://kodtest-server-01.herokuapp.com/"
    }
    
    var path: String {
        
        return ""
    }
    
    var httpMethod: HTTPMethod {
        
        return .GET
    }
    
    var parameters: [String : Any] {
        
        var params = [String: String]()
        params["duration"] = "2000"
        params["delay"] = "100"
        params["offset"] = "0"

        switch self {
            
        case .offset(let offSet):
            
            if let offSet = offSet, offSet.isEmpty == false {
            
                params["offset"] = offSet
            }
        }
        
        return params
    }
}

