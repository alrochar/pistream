//
//  EndpointType.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 05/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import Foundation


enum HTTPMethod: String {
    
    case GET
}

protocol Service {
    
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var parameters: [String: Any] { get }
}

extension Service {
    
    public var urlRequest: URLRequest {
        
        guard let url = self.url else {
            
            fatalError()
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.GET.rawValue
        
        return request
    }
    
    private var url: URL? {
        
        var urlComponents = URLComponents(string: baseURL)
        
        urlComponents?.path = path
        
        guard let params = parameters as? [String: String] else {
            
            fatalError()
        }
        
        urlComponents?.queryItems = params.map { URLQueryItem(name: $0.key, value: $0.value) }
        
        return urlComponents?.url
    }
}
