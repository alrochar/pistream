//
//  ViewController+Persistance.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 06/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import UIKit
import CoreData

extension ViewController {
    
    func save(number: Int64) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        if let entity = NSEntityDescription.entity(forEntityName: "Row", in: managedContext) {
            
            let numberIndex = NSManagedObject(entity: entity, insertInto: managedContext)
            
            numberIndex.setValue(number, forKey: "index")
        }
        
        do {
            
            try managedContext.save()
            
        } catch let error as NSError {
            
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func fetchPersistentData() -> [String] {
        
        var numbers = [String]()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            
            return numbers
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Row>(entityName: "Row")
        
        do {
            let fetchedResults = try managedContext.fetch(fetchRequest)
            
            if fetchedResults.count > 0 {
                
                numbers = fetchedResults.map { ("\($0.index)")}
                
                return numbers
            }
            
        } catch let error as NSError {
            // something went wrong, print the error.
            print(error.description)
        }
        
        return numbers
    }
    
    func deleteAllData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            
            return
        }
        
        let context:NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Row")
        
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            
            let results = try context.fetch(fetchRequest)
            
            for managedObject in results {
                
                if let managedObjectData = managedObject as? NSManagedObject {
                
                    context.delete(managedObjectData)
                }
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            try managedContext.save()
            
        } catch let error as NSError {
            print("Deleted all my data in myEntity error : \(error) \(error.userInfo)")
        }
    }
}
