//
//  ViewController.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 04/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    private lazy var startButton: UIButton = {
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var stopButton: UIButton = {
         
         let button = UIButton()
         button.translatesAutoresizingMaskIntoConstraints = false
         return button
     }()
    
    lazy var deleteButton: UIButton = {
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var items: [String] = []
    private let activityIndicator = CustomLoadingIndicator()
    private var provider = ServiceProvider<APIHeroku>()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configureSubViews()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.items = self.fetchPersistentData()
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") else {
            
            return UITableViewCell()
        }
        
        cell.textLabel?.text = "Number 3 was found at offset \(self.items[indexPath.row])"
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count;
    }
    
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        print("You selected cell #\(indexPath.row)!")

    }
}

extension ViewController: APIDataDelegate {
    
    func requestData(offset: String?, duration: String) {
        
        self.provider.load(service: .offset(offset: offset)) { (result) in
        
            switch result {
                
            case .success(let numbers):
                self.activityIndicator.isHidden = true
                self.handleDataResponse(numbers: numbers as! String)
                
            case .failure(let error):
                self.activityIndicator.stopAnimating()
                self.updateButtonStatus(hideStart: false, hideStop: true)
                print(error)
            
            case .empty:
                print("no data")
            }
        }
    }
    
    func cancelRequest() {
        
        self.provider.cancel()
    }
}

private extension ViewController {
    
    func handleDataResponse(numbers: String) {
        
        var indexes = [String]()
        
        let numbersArray = numbers.map {  String($0) }
        
        for (index, _) in numbersArray.enumerated() {
            
            if numbersArray[index] == "3" {

                let latestIndex = (self.items.isEmpty == false) ? (Int(self.items.last ?? "0") ?? 0) + index + 1 : index
                
                self.save(number: Int64(latestIndex))
                
                indexes.append("\(latestIndex)")
            }
        }
        
        self.items += indexes
        self.activityIndicator.stopAnimating()
        self.updateButtonStatus(hideStart: false, hideStop: true)
        self.tableView.reloadData()
    }
    
    @objc func buttonStartClicked() {
        
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false

        let offset: String = self.items.last ?? "0"
        
        self.requestData(offset: String(describing: offset), duration: "1000")
        self.updateButtonStatus(hideStart: true, hideStop: false)
    }
    
    @objc func buttonStopClicked() {
        
        self.cancelRequest()
        
        self.activityIndicator.isHidden = true
        self.updateButtonStatus(hideStart: false, hideStop: true)
    }
    
    func updateButtonStatus(hideStart: Bool, hideStop: Bool) {
        
        self.stopButton.isHidden = hideStop
        self.startButton.isHidden = hideStart
    }

    @objc func deleteAllDataConfirm() {
        
        let alert = UIAlertController(title: "Delete Data", message: "Are you sure you want to delete it", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            
            switch action.style {
                
            case .default:
                
                self.items = []
                self.tableView.reloadData()
                self.deleteAllData()
                
            case .cancel:
                
                alert.dismiss(animated: true) {}
                
            case .destructive:
                print("destructive")
                
            default:
                fatalError()
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController {
    
    func configureSubViews() {
        
        self.addSubviews()
        self.configureConstraints()
        
        self.startButton.setTitle("Start", for: .normal)
        self.startButton.backgroundColor = .systemBlue
        self.startButton.addTarget(self, action: #selector(self.buttonStartClicked), for: .touchUpInside)
        
        self.stopButton.setTitle("Stop", for: .normal)
        self.stopButton.backgroundColor = .systemGray
        self.stopButton.addTarget(self, action: #selector(self.buttonStopClicked), for: .touchUpInside)
        
        self.deleteButton.setTitle("Delete", for: .normal)
        self.deleteButton.backgroundColor = .systemRed
        self.deleteButton.addTarget(self, action: #selector(self.deleteAllDataConfirm), for: .touchUpInside)
        
        self.activityIndicator.isHidden = true
        
    }
    
    func addSubviews() {
        
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.stopButton)
        self.view.addSubview(self.startButton)
        self.view.addSubview(self.activityIndicator)
        self.view.addSubview(self.deleteButton)
    }
    
    func configureConstraints() {
        
        NSLayoutConstraint.activate([
            
            //TableView
            self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -90),
            
            //StartButton
            self.startButton.bottomAnchor.constraint(equalTo: self.deleteButton.topAnchor),
            self.startButton.heightAnchor.constraint(equalToConstant: 45.0),
            self.startButton.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            
            //StopButton
            self.stopButton.bottomAnchor.constraint(equalTo: self.deleteButton.topAnchor),
            self.stopButton.heightAnchor.constraint(equalToConstant: 45.0),
            self.stopButton.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            
            //Activity Indicator
            self.activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            
            //Delete button
            self.deleteButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.deleteButton.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            self.deleteButton.heightAnchor.constraint(equalToConstant: 45.0)
        ])
    }

}
