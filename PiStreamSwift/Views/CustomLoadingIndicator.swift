//
//  CustomLoadingIndicator.swift
//  PiStreamSwift
//
//  Created by Alejandro Rocha on 04/03/2020.
//  Copyright © 2020 Alejandro Rocha. All rights reserved.
//

import UIKit

class CustomLoadingIndicator: UIView {
    
    private let loadingIndicator = UIActivityIndicatorView(style: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubview() {
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.addSubview(loadingIndicator)
        self.loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        layer.cornerRadius = 20
    }
    
    func startAnimating() {
        
        loadingIndicator.startAnimating()
    }
    
    func stopAnimating() {
        
        loadingIndicator.stopAnimating()
    }
}
